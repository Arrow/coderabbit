package com.hypnorabbit.windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class ProjectsDialog extends JDialog {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ProjectsDialog dialog = new ProjectsDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ProjectsDialog() {
		initGUI();
	}
	@SuppressWarnings("unchecked")
	private void initGUI() {
		setTitle("codeRabbit - Projects");
		setResizable(false);
		setBounds(100, 100, 229, 240);
		getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnProject = new JMenu("Project");
		menuBar.add(mnProject);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mnProject.add(mntmNew);
		
		JMenu mnSelected = new JMenu("Selected");
		mnProject.add(mnSelected);
		
		JMenuItem mntmEdit = new JMenuItem("Edit");
		mnSelected.add(mntmEdit);
		
		JMenuItem mntmDelete = new JMenuItem("Delete");
		mnSelected.add(mntmDelete);
		{
			JButton okButton = new JButton("Done");
			okButton.setBounds(10, 142, 96, 34);
			getContentPane().add(okButton);
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.setBounds(117, 142, 96, 34);
		getContentPane().add(btnNewButton_1);
		
		JList list = new JList();
		list.setBounds(10, 11, 203, 118);
		getContentPane().add(list);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"Test - C:\\.\\.\\.\\projects\\Test", "Test1 - C:\\.\\.\\.\\projects\\Test1", "Test2 - C:\\.\\.\\.\\projects\\Test2", "Test3 - C:\\.\\.\\.\\projects\\Test3"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
	}
}
