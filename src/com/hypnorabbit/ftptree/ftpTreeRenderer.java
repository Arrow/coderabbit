package com.hypnorabbit.ftptree;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;


public class ftpTreeRenderer extends DefaultTreeCellRenderer {
	
	private ImageIcon iconProject = new ImageIcon(getClass().getResource("/folder.png"));
	private ImageIcon iconSource = new ImageIcon(getClass().getResource("/folder.png"));
	private ImageIcon iconPackage = new ImageIcon(getClass().getResource("/folder.png"));
	private ImageIcon iconClass = new ImageIcon(getClass().getResource("/folder.png"));
	private ImageIcon iconFolder = new ImageIcon(getClass().getResource("/folder.png"));

	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				row, hasFocus);

		ftpItemNode node = (ftpItemNode) value;

		switch (node.getType()) {
		case ftpItemNode.NODE_PROJECT:
			setIcon(iconProject);
			break;
		case ftpItemNode.NODE_SOURCE:
			setIcon(iconSource);
			break;
		case ftpItemNode.NODE_PACKAGE:
			setIcon(iconPackage);
			break;
		case ftpItemNode.NODE_CLASS:
			setIcon(iconClass);
			break;
		case ftpItemNode.NODE_FOLDER:
			setIcon(iconFolder);
			break;
		}

		return this;
	}
}