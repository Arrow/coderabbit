package com.hypnorabbit.docking;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.flexdock.docking.Dockable;
import org.flexdock.docking.DockingPort;
import org.flexdock.docking.defaults.DefaultDockingPort;
import org.pushingpixels.substance.api.DecorationAreaType;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

import com.hypnorabbit.ftptree.ftpItemNode;

public class DockableJTree extends JTree {
	
	public DockableJTree(ftpItemNode node) {
		super(node);
		this.setVisible(true);
		this.setSize(400, 400);
	}
	
    public void doLayout() {
        Insets in = getInsets();
        if(titlebar != null) {
        	titlebar.setBounds(in.left, in.top, getWidth()-in.left-in.right, 25);
        } else {
        	System.out.println("oops");
        }
    }
    
	public DockingPort dockingPort;
    public JLabel titlebar;
    private Dockable dockableImpl;
    
    public DockingPort createDockingPort() {
    	DefaultDockingPort port = new DefaultDockingPort();
        port.setBackground(Color.gray);
        port.setPreferredSize(new Dimension(100, 100));
        return port;
    }
    
    public JLabel createTitleBar(String title) {
    	JLabel label = new JLabel(title);
    	SubstanceLookAndFeel.setDecorationType(label, DecorationAreaType.HEADER);
        label.setOpaque(true);
		return label;
    }
    
    public Dockable getDockable() {
        return dockableImpl;
    }
    
    public void setDockableImpl(Dockable dockableImpl) {
    	this.dockableImpl = dockableImpl;
    }
}
