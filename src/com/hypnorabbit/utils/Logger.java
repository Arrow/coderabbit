package com.hypnorabbit.utils;

public class Logger {
	
	public static int VERBOSE_MODE = 0;
	public static int NOTIFY_MODE = 1;
	public static int WARNING_MODE = 2;
	public static int ERROR_MODE = 3;
	
	public int CURRENT_MODE = 0;
	
	public static int ERROR = 0;
	public static int NOTE = 1;
	
	public Logger(int mode) {
		if(mode > 3) {
			throw new IndexOutOfBoundsException("Logger mode must be between 0-3");
		} else {
			this.CURRENT_MODE = mode;
		}
	}
	
	public void Logln(Object obj, int mode, int type) {
		boolean doLog = true;
		
		switch(mode) {
		case 0: //VERBOSE
			if(this.CURRENT_MODE == Logger.VERBOSE_MODE)
				doLog = true;
			else 
				doLog = false;
			break;
		case 1: //NOTIFY
			if(this.CURRENT_MODE == Logger.NOTIFY_MODE || this.CURRENT_MODE == Logger.VERBOSE_MODE)
				doLog = true;
			else 
				doLog = false;
			break;
		case 2: //WARNING
			if(this.CURRENT_MODE == Logger.WARNING_MODE || this.CURRENT_MODE == Logger.VERBOSE_MODE)
				doLog = true;
			else 
				doLog = false;
			break;
		case 3:
			if(this.CURRENT_MODE == Logger.NOTIFY_MODE || this.CURRENT_MODE == Logger.VERBOSE_MODE)
				doLog = true;
			else 
				doLog = false;
			break;
		}
		
		if(doLog) {
			boolean logMore = true;
			
			if(obj.getClass().getName().toLowerCase().contains("exception")) {
				
				Throwable exception = (Throwable)obj;
				StackTraceElement[] elements = (StackTraceElement[]) exception.getStackTrace();
				System.err.println(exception.getClass().toString().replace("class ", "") + ": " + exception.getMessage());
				
				for (StackTraceElement element : elements) {
					System.err.println("	" + element);
				}
				logMore = false;
			}
			
			if(logMore) {
				if(type == Logger.ERROR) {
					System.err.println(obj.toString());
				} else {
					System.out.println(obj.toString());
				}
			}
			//TODO Log to a special window or something
			
		}
	}
}
