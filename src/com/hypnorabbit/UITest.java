package com.hypnorabbit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.SwingConstants;
import javax.swing.Box;
import java.awt.Component;
import java.awt.GridLayout;
import java.io.File;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class UITest extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UITest frame = new UITest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UITest() {
		createContents();
	}
	
	private void createContents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 733, 576);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		
		
		
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		contentPane.add(lblNewLabel);
		final JPanel housingPane = new JPanel();
		housingPane.setBorder(null);
		housingPane.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		housingPane.setBorder(new LineBorder(Color.green));
		EditorPane editorPane = new EditorPane(new File("C:\\Users\\Andrew\\Documents\\changes.txt"));
		editorPane.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		housingPane.add(editorPane);
		contentPane.add(housingPane);
		this.ep = editorPane;
		this.addComponentListener(new ComponentListener() {
			@Override
			public void componentHidden(ComponentEvent e) {}
			@Override
			public void componentMoved(ComponentEvent e) {}
			@Override
			public void componentResized(ComponentEvent e) {
				UITest frame = (UITest)e.getComponent();
				Insets in = frame.getContentPane().getInsets();
				
				housingPane.setSize(frame.getContentPane().getWidth()-in.left-in.left, frame.getContentPane().getHeight()-25-in.top);
				
				Insets in2 = frame.ep.getInsets();
				
				frame.ep.setPreferredSize(new Dimension(housingPane.getWidth()-in2.left-in2.left-in.left-in.left, housingPane.getHeight()-in2.top-in2.top-in.top-in.top));
			}
			@Override
			public void componentShown(ComponentEvent e) {}
		});
	}
	
	public EditorPane ep;

}
