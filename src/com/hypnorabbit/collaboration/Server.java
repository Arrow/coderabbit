package com.hypnorabbit.collaboration;

import java.io.*;
import java.net.*;

import com.hypnorabbit.EditorPane;

public class Server {
	
	public ServerSocket providerSocket;
	public Socket connection = null;
	public ObjectOutputStream out;
	public ObjectInputStream in;
	public String message;
	public EditorPane editorPane;
	
	public Server(EditorPane editorPane) {
		this.editorPane = editorPane;
		try{
			//1. creating a server socket
			providerSocket = new ServerSocket(6464, 10);
			//2. Wait for connection
			System.out.println("Waiting for connection");
			connection = providerSocket.accept();
			System.out.println("Connection received from " + connection.getInetAddress().getHostName());
			//3. get Input and Output streams
			out = new ObjectOutputStream(connection.getOutputStream());
			out.flush();
			in = new ObjectInputStream(connection.getInputStream());
			sendData("Connection successful");
			//4. The two parts communicate via the input and output streams
			do {
				try {
					message = (String)in.readObject();
					System.out.println("client>" + message);
					editorPane.getTextArea().append("\nclient> " + message);
					//if (message.equals("bye"))
						//sendData("bye");
				} catch(ClassNotFoundException classnot) {
					System.err.println("Data received in unknown format");
				}
			} while(!message.equals("bye"));
		} catch(IOException ioException) {
			ioException.printStackTrace();
		} finally {
			//4: Closing connection
			try {
				in.close();
				out.close();
				providerSocket.close();
			} catch(IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
	
	public void sendData(String data) {
		try{
			out.writeObject(data);
			out.flush();
			System.out.println("server>" + data);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
}
