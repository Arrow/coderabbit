package com.hypnorabbit;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.Gutter;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.flexdock.docking.Dockable;
import org.flexdock.docking.DockingConstants;
import org.flexdock.docking.DockingPort;
import org.flexdock.docking.defaults.AbstractDockable;
import org.flexdock.docking.defaults.DefaultDockingPort;
import org.pushingpixels.substance.api.DecorationAreaType;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

import com.hypnorabbit.listeners.EditorKeyListener;


public class EditorPane extends JRootPane implements HyperlinkListener, SyntaxConstants, DockingConstants {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private RTextScrollPane scrollPane;

	private RSyntaxTextArea textArea;
	private AutoCompletion ac;
	
	private boolean saved = false;
	private EditorKeyListener editorKeyListener;
	private File file;
	private boolean hasSaveLocation = false;
	
	@Override
	public Component.BaselineResizeBehavior getBaselineResizeBehavior() {
	    return Component.BaselineResizeBehavior.CONSTANT_ASCENT;
	}

	@Override
	public int getBaseline(int width, int height) {
	    return 0;
	}
	
	public EditorPane(File file) {
		
		//
		this.file = file;
		textArea = createTextArea();
		setRemoteText(file.getAbsolutePath());
		textArea.setSyntaxEditingStyle(SYNTAX_STYLE_PHP); //TODO: Make dynamic
		scrollPane = new RTextScrollPane(textArea, true);
		
		Gutter gutter = scrollPane.getGutter();
		gutter.setBookmarkingEnabled(true);
		gutter.setBookmarkIcon(new ImageIcon(getClass().getClassLoader().getResource("bookmark.png")));
		getContentPane().add(scrollPane);
		
		CompletionProvider provider = createCompletionProvider();
		
		ac = new AutoCompletion(provider);
		ac.setChoicesWindowSize(350, 200);
		ac.setAutoCompleteEnabled(true);
		ac.install(textArea);
		ac.setAutoActivationDelay(20);
		ac.setAutoActivationEnabled(true);
		ac.setDescriptionWindowSize(340, 220);
		ac.setShowDescWindow(true);
		this.setTheme("Dark", "/themes/dark.xml"); //TODO: Make dynamic
		
		editorKeyListener = new EditorKeyListener(this, file);
		textArea.addKeyListener(editorKeyListener);
		
		setBorder(new LineBorder(Color.red));//TODO: Debug
		

	}
	
	public void setSaved(boolean saved) {
		this.saved = saved;
		if(saved) {
			
		}
	}
	
	private CompletionProvider createCompletionProvider() {
		DefaultCompletionProvider provider = new DefaultCompletionProvider();
		
		try {
			provider.loadFromXML(Class.class.getResourceAsStream("/php.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		provider.setAutoActivationRules(false, ">:$");
		provider.setMultiAutoActivationRules(false, "->");
		
		return provider;
	}
	
	/**
	 * Creates the text area for this application.
	 *
	 * @return The text area.
	 */
	private RSyntaxTextArea createTextArea() {
		RSyntaxTextArea textArea = new RSyntaxTextArea(25, 70);
		textArea.setCaretPosition(0);
		textArea.addHyperlinkListener(this);
		textArea.requestFocusInWindow();
		textArea.setMarkOccurrences(true);
		textArea.setCodeFoldingEnabled(true);
		textArea.setClearWhitespaceLinesEnabled(false);
		return textArea;
	}
	
	/**
	 * Sets the content in the text area to that in the specified resource.
	 *
	 * @param resource The resource to load.
	 */
	@SuppressWarnings("unused")
	private void setText(String resource) {
		ClassLoader cl = getClass().getClassLoader();
		BufferedReader r = null;
		try {
			r = new BufferedReader(new InputStreamReader(cl.getResourceAsStream(resource), "UTF-8"));
			
			textArea.read(r, null);
			r.close();
			textArea.setCaretPosition(0);
			textArea.discardAllEdits();
		} catch (RuntimeException re) {
			throw re; // FindBugs
		} catch (Exception e) { // Never happens
			//textArea.setText("Type here to see syntax highlighting");
		}
	}
	
	private void setRemoteText(String path) {
		BufferedReader r = null;
		try {
			if(path == null) {
				
			}
			r = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));
			textArea.read(r, null);
			r.close();
			textArea.setCaretPosition(0);
			textArea.discardAllEdits();
		} catch (RuntimeException re) {
			throw re; // FindBugs
		} catch (Exception e) { // Never happens
			textArea.setText("");
		}
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
		if (e.getEventType()==HyperlinkEvent.EventType.ACTIVATED) {
			URL url = e.getURL();
			if (url==null) {
				UIManager.getLookAndFeel().provideErrorFeedback(null);
			} else {
				if(Desktop.isDesktopSupported()) {
					try {
					  Desktop.getDesktop().browse(url.toURI());
					} catch (IOException | URISyntaxException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}
	
	public void setTheme(String name, String xml) {
		new ThemeAction(name, xml).set();
	}
	
	private class ThemeAction extends AbstractAction {

		private static final long serialVersionUID = 1L;
		private String xml;
		
		public ThemeAction(String name, String xml) {
			putValue(NAME, name);
			this.xml = xml;
		}
		
		public void set() {
			InputStream in = getClass().getResourceAsStream(xml);
			try {
				Theme theme = Theme.load(in);
				theme.apply(textArea);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		
		public void actionPerformed(ActionEvent e) {
			InputStream in = getClass().getResourceAsStream(xml);
			try {
				Theme theme = Theme.load(in);
				theme.apply(textArea);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
    
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public boolean hasSaveLocation() {
		return hasSaveLocation;
	}

	public void setHasSaveLocation(boolean hasSaveLocation) {
		this.hasSaveLocation = hasSaveLocation;
	}

	public RTextScrollPane getScrollPane() {
		return scrollPane;
	}

	public RSyntaxTextArea getTextArea() {
		return textArea;
	}

	public AutoCompletion getAc() {
		return ac;
	}

	public boolean isSaved() {
		return saved;
	}

	public EditorKeyListener getEditorKeyListener() {
		return editorKeyListener;
	}

}
