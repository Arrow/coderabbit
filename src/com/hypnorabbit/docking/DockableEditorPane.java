package com.hypnorabbit.docking;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.FlowLayout;

import org.flexdock.docking.Dockable;
import org.flexdock.docking.DockingManager;
import org.flexdock.docking.DockingPort;
import org.flexdock.docking.defaults.DefaultDockingPort;
import org.flexdock.docking.event.DockingEvent;
import org.flexdock.docking.event.DockingListener;

import com.hypnorabbit.EditorPane;

public class DockableEditorPane {
	
	private JPanel housingPanel; //The panel used to house the TitleBar and the EditorPane
	private JPanel editorPaneWrapper;
	private EditorPane editorPane;
	private DockableTitleBar titleBar;
	private ComponentListener componentListener;
	
	@SuppressWarnings("unchecked")
	public DockableEditorPane(String title, String tabText, DefaultDockingPort globalDockingPort, File file) {
		housingPanel = new JPanel();
		housingPanel.setLayout(new BoxLayout(housingPanel, BoxLayout.PAGE_AXIS));
		
		titleBar = new DockableTitleBar(title);
		//titleBar.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		
		editorPaneWrapper = new JPanel();
		editorPaneWrapper.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		editorPaneWrapper.setBorder(new LineBorder(Color.green)); //TODO: Debug
		//((FlowLayout)editorPaneWrapper.getLayout()).setVgap(0);
		
		editorPane = new EditorPane(file);
		editorPane.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		
		editorPaneWrapper.add(editorPane);
		
		housingPanel.add(titleBar);
		housingPanel.add(editorPaneWrapper);
		
		Dockable dockablePane = DockingManager.registerDockable(housingPanel, tabText);
		dockablePane.getDragSources().add(titleBar);
		dockablePane.getDragSources().add(editorPane);
		dockablePane.getDragSources().add(editorPane.getTextArea());
		DockingManager.dock(housingPanel, (DockingPort)globalDockingPort);
		
		org.flexdock.docking.floating.policy.FloatPolicyManager.setGlobalFloatingEnabled(true);
		
		editorPane.setPreferredSize(new Dimension(editorPaneWrapper.getWidth(), editorPaneWrapper.getHeight()));
		
		componentListener = new ComponentListener() {
			@Override
			public void componentHidden(ComponentEvent e) {}
			@Override
			public void componentMoved(ComponentEvent e) {}
			@Override
			public void componentResized(ComponentEvent e) {
				editorPane.setSize(new Dimension(editorPaneWrapper.getWidth(), editorPaneWrapper.getHeight()-1));
				editorPane.setPreferredSize(new Dimension(editorPaneWrapper.getWidth(), editorPaneWrapper.getHeight()-1));
				
			}
			@Override
			public void componentShown(ComponentEvent e) {}
		};
		
		globalDockingPort.addComponentListener(componentListener);
		//editorPaneWrapper.addComponentListener(componentListener);
		housingPanel.addComponentListener(componentListener);
	}
	
	public EditorPane getEditorPane() {
		return this.editorPane;
	}
	
	public JPanel getHousingPanel() {
		return this.housingPanel;
	}
	
	public JPanel getEditorPaneWrapper() {
		return this.editorPaneWrapper;
	}
	
	public DockableTitleBar getTitleBar() {
		return this.titleBar;
	}
}
