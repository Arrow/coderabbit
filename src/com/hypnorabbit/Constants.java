package com.hypnorabbit;

public class Constants {
	
	public static String applicationName = "CodeRabbit";
	
	public static String defaultDirectory = System.getProperty("user.home") + "\\My Documents";
	public static String workingDirectory = defaultDirectory + "\\CodeRabbit";
	public static String ftpLocalStorage = workingDirectory + "\\ftptemp";
	public static String projectDirectory = workingDirectory + "\\projects";
	public static String tempDirectory = workingDirectory + "\\temp";
	
	
	public static String propertiesFile = workingDirectory + "\\" + applicationName + ".properties";
	public static String changesFile = workingDirectory + "\\changes.txt";
	
}
