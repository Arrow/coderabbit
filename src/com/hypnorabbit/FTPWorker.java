package com.hypnorabbit;

import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

@SuppressWarnings("unused")
public class FTPWorker {

	private FTPClient ftp = null;
	private String host;
	private int port;
	private String username;
	private String password;
	private String default_path;
	
	private String current_path;

	public FTPWorker() {
		this.ftp = new FTPClient();
	}

	public void connect(String host, int port, String username, String password, String default_path) throws SocketException, IOException, BadLoginInfoException {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.default_path = default_path;
		ftp.connect(host);
		
		if (!ftp.login(username, password)) {
			ftp.logout();
			throw new BadLoginInfoException("Failed to login with " + username + ":" + password.substring(0, 4) + "...@" + host + ":" + String.valueOf(port));
		}
		
		int reply = ftp.getReplyCode();
		
		if (!FTPReply.isPositiveCompletion(reply)) { // FTPReply stores a set of constants for FTP reply codes.
			ftp.disconnect();
		}
		
        ftp.enterLocalPassiveMode();//enter passive mode
        System.out.println("Remote system is " + ftp.getSystemType());//get system name
        
        if(default_path != null) {
        	this.setPath(default_path);
        } else {
        	//this.setPath("/");
        }
	}
	
	public FTPClient getFTPClient() {
		return this.ftp;
	}
	
	public void setPath(String path) {
        try {
			ftp.changeWorkingDirectory(path);
			this.current_path = path;
			System.err.println("Current directory set to: " + ftp.printWorkingDirectory());
		} catch (IOException e) {
			e.printStackTrace();// TODO Auto-generated catch block
		}
	}

	public FTPFile[] getAllFolders(String path) {
		try {
			if(path == null) {
				//System.err.println("getAllFolder() null path");
				FTPFile[] ftpDirs = ftp.listDirectories();
				return ftpDirs;
			} else {
				this.setPath(path);
				//ftp.changeWorkingDirectory(path);
				//FTPFile[] ftpFiles = ftp.listFiles();
				return ftp.listFiles();
			}
		} catch (IOException e) {
			e.printStackTrace();// TODO Auto-generated catch block
		}
		return null;
	}

	public FTPFile[] getAllFiles(String path) {
		try {
			if(path == null) {
				FTPFile[] ftpFiles = ftp.listFiles();
				return ftpFiles;
			} else {
				this.setPath(path);
				FTPFile[] ftpFiles = ftp.listFiles();
				return ftpFiles;
			}
		} catch (IOException e) {
			e.printStackTrace();// TODO Auto-generated catch block
		}
		return null;
	}
	
	public void getFile(String filename, OutputStream stream) {
		try {
			this.ftp.retrieveFile(filename, stream);
		} catch (IOException e) {
			e.printStackTrace(); // TODO Auto-generated catch block
		}
	}
	
	public String getHost() {
		return this.host;
	}
	
	public String getCurrentDirectory() {
		return this.current_path;
	}
	
	public String getDefaultDirectory() {
		return this.default_path;
	}
	
	

	class BadLoginInfoException extends Exception {

		private static final long serialVersionUID = 1L;
		private String message = null;
		
		public BadLoginInfoException(String message) {
			super(message);
			this.message = message;
		}

		@Override
		public String toString() {
			return message;
		}

		@Override
		public String getMessage() {
			return message;
		}
	}
}
