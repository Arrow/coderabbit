package com.hypnorabbit.docking;

import java.awt.Component;

import org.flexdock.docking.defaults.AbstractDockable;

public class DockableImpl extends AbstractDockable {
	
	private Component titleBar;
	
    @SuppressWarnings("unchecked")
	public DockableImpl(String title, Component titleBar) {
        super("dockable." + title);
        getDragSources().add(titleBar);
        this.titleBar = titleBar;
        setTabText(title);
    }

	@Override
	public Component getComponent() {
		return this.titleBar;
	}
    
}
