package com.hypnorabbit.docking;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.flexdock.docking.Dockable;
import org.flexdock.docking.DockingPort;
import org.flexdock.docking.defaults.DefaultDockingPort;
import org.pushingpixels.substance.api.DecorationAreaType;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

public class DockablePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	public DockingPort dockingPort;
    public JLabel titleBar;
    private Dockable dockableImpl;
    
    public DockablePanel(String title) {
    	this.titleBar = createTitleBar(title);
    	this.add(titleBar);
    }
    
    public void doLayout() {
        Insets in = getInsets();
        titleBar.setBounds(in.left, in.top, getWidth()-in.left-in.right, 25);
    }
    
    public DefaultDockingPort createDockingPort() {
    	DefaultDockingPort port = new DefaultDockingPort();
        port.setBackground(Color.blue);
        port.setPreferredSize(new Dimension(100, 100));
        return port;
    }
    
    public JLabel createTitleBar(String title) {
    	JLabel label = new JLabel(title);
    	SubstanceLookAndFeel.setDecorationType(label, DecorationAreaType.HEADER);
        label.setOpaque(true);
		return label;
    }
    
    public Dockable getDockableImpl() {
        return dockableImpl;
    }
    
    public void setDockableImpl(Dockable dockableImpl) {
    	this.dockableImpl = dockableImpl;
    }
}
