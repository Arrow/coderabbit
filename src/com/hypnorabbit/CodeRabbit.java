package com.hypnorabbit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.MenuBarUI;
import javax.swing.UIManager;
import javax.swing.JMenuBar;

import org.flexdock.docking.Dockable;
import org.flexdock.docking.DockingConstants;
import org.flexdock.docking.DockingManager;
import org.flexdock.docking.DockingPort;
import org.flexdock.docking.defaults.AbstractDockable;
import org.flexdock.docking.defaults.DefaultDockingPort;

import com.hypnorabbit.docking.DockableEditorPane;
import com.hypnorabbit.docking.DockableImpl;
import com.hypnorabbit.docking.DockableJTree;
import com.hypnorabbit.docking.DockablePanel;
import com.hypnorabbit.docking.DockableTitleBar;
import com.hypnorabbit.ftptree.ftpItemNode;
import com.hypnorabbit.utils.Logger;

public class CodeRabbit extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static Logger logger = new Logger(Logger.VERBOSE_MODE);
	private JTree ftpTree;
	private JTabbedPane tabbedPane;
	private FTPWorker ftpWorker = new FTPWorker();
	private Properties config;
	private ftpItemNode rootNode;
	private DefaultDockingPort globalPort;
	final JFileChooser fileChooser = new JFileChooser();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		//System.setProperty("sun.java2d.opengl", Boolean.TRUE.toString());
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				File docFolder = new File(Constants.workingDirectory);
				docFolder.mkdirs();
				// Create properties file if it doesn't exist
				File properties = new File(Constants.propertiesFile);
				Properties config = new Properties();
				if(properties.exists()) {
					System.out.println("Properties file found at: " + properties.getAbsolutePath()); // TODO: log file
				} else {
					try {
						properties.createNewFile();
						getLogger().Logln("Properties file created at: " + properties.getAbsolutePath(), Logger.ERROR_MODE, Logger.NOTE); // TODO: log file
					} catch (IOException e) {
						getLogger().Logln(e, Logger.ERROR_MODE, Logger.ERROR);
					}
				}
				try {
					config.load(new FileInputStream(Constants.propertiesFile));
				} catch (IOException e3) {
					e3.printStackTrace();// TODO Auto-generated catch block
				}
				try {
					UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
					if(config.getProperty("look_and_feel") == null) {
						config.setProperty("look_and_feel", "org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
					}
				} catch (Throwable e) {
					getLogger().Logln("Substance failed to initialize: ", Logger.ERROR_MODE, Logger.ERROR);
					getLogger().Logln(e, Logger.ERROR_MODE, Logger.ERROR);
				}
				
				
				if(config.getProperty("window_width") == null) {
					config.setProperty("window_width", "500");
					config.setProperty("window_height", "400");
					config.setProperty("window_x", "50");
					config.setProperty("window_y", "50");
				}

				OutputStream os;
				try {
					os = new FileOutputStream(Constants.propertiesFile);
					config.store(os, null);
					os.close();
					System.out.println("Saving default size");
				} catch (IOException e) {
					System.err.println("Failed to save default sizes");
					e.printStackTrace();// TODO Auto-generated catch block
				}
				
				final CodeRabbit frame = new CodeRabbit();
				
				String os1 = System.getProperty("os.name");

                if (os1.equals("Mac OS X")) {
                    try {
                        System.setProperty("apple.laf.useScreenMenuBar", "true");
                        frame.menuBar.setUI((MenuBarUI) Class.forName("com.apple.laf.AquaMenuBarUI").newInstance());
                    } catch (Exception ex) {
                        // log...
                    }
                }
				
				CodeRabbit.setLaF(frame.getConfig().getProperty("look_and_feel").trim());
				
				System.out.println("Root document folder: " + Constants.defaultDirectory); // TODO: log file

				File dir = new File(Constants.workingDirectory);
				dir.mkdirs();
				
				

				File ftpDir = new File(Constants.ftpLocalStorage);
				ftpDir.mkdir();
				
				
				ClassLoader cl = getClass().getClassLoader();
				File changesFile = new File(Constants.workingDirectory + "\\changes.txt");
				try {
					changesFile.createNewFile();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				FileOutputStream fileOS = null;
				try {
					fileOS = new FileOutputStream(changesFile.getAbsolutePath());
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(cl.getResourceAsStream("changes.txt"), "UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					while(reader.ready()) {
						byte[] line = reader.readLine().getBytes();
						byte[] newLine = "\r\n".getBytes();
						byte[] completeLine = new byte[line.length + newLine.length];
						System.arraycopy(line, 0, completeLine, 0, line.length);
						System.arraycopy(newLine, 0, completeLine, line.length, newLine.length);
						fileOS.write(completeLine);
					}
					reader.close();
					fileOS.close();
				} catch (IOException e1) {
					e1.printStackTrace();// TODO Auto-generated catch block
				}
				

				//EditorPane editorPane = new EditorPane(changesFile);
				//frame.tabbedPane.addTab("*" + changesFile.getName(), editorPane);
				
				frame.addComponentListener(new ComponentListener() {
					public void componentResized(ComponentEvent evt) {
						// When the window is resized, adjust the tabbedPane size
						Component cmp = (Component) evt.getSource();
						frame.getConfig().setProperty("window_width", String.valueOf(cmp.getWidth()));
						frame.getConfig().setProperty("window_height", String.valueOf(cmp.getHeight()));
						OutputStream os;
						try {
							os = new FileOutputStream(Constants.propertiesFile);
							frame.getConfig().store(os, null);
							os.close();
							//System.out.println("Setting size");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						frame.testPanel.setPreferredSize(new Dimension(frame.testPanel.getParent().getWidth(), frame.testPanel.getParent().getHeight()));
						//frame.testPanel2.setPreferredSize(new Dimension(frame.testPanel2.getParent().getWidth(), frame.testPanel2.getParent().getHeight()));
					}

					@Override
					public void componentHidden(ComponentEvent arg0) {}

					@Override
					public void componentMoved(ComponentEvent e) {
						frame.getConfig().setProperty("window_x", String.valueOf(frame.getX()));
						frame.getConfig().setProperty("window_y", String.valueOf(frame.getY()));
						OutputStream os;
						try {
							os = new FileOutputStream(Constants.propertiesFile);
							frame.getConfig().store(os, null);
							os.close();
							//System.out.println("saving window position");
						} catch (IOException e2) {
							e2.printStackTrace();// TODO Auto-generated catch block
						}
						//frame.testPanel.setSize(frame.testPanel.getParent().getSize());
					}

					@Override
					public void componentShown(ComponentEvent arg0) {}
				});
				
				
				/* We have to run this stuff down here because the window has it's size set somewhere */
				int winWidth = Integer.valueOf(config.getProperty("window_width").trim());
				int winHeight = Integer.valueOf(config.getProperty("window_height").trim());
				frame.setSize(winWidth, winHeight);
				System.out.println("Setting Width:" + winWidth + " Height:" + winHeight); //TODO: logger
				
				//frame.pack();
				frame.setVisible(true);
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("unchecked")
	public CodeRabbit() {
		this.config = new Properties();
		try {
			this.config.load(new FileInputStream(Constants.propertiesFile));
		} catch (IOException e3) {
			e3.printStackTrace();// TODO Auto-generated catch block
		}
		setTitle("CodeRabbit");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 615, 418);
		
		
		contentPane = new JPanel(new BorderLayout());
		createMenu();
		
		
		
		this.rootNode = new ftpItemNode("FTP Servers", ftpItemNode.NODE_FOLDER);

		this.ftpTree = new JTree(this.rootNode);
		JScrollPane qPane = new JScrollPane(this.ftpTree, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.ftpTree.setScrollsOnExpand(true);
		this.ftpTree.setShowsRootHandles(true);
		//getContentPane().add(qPane);
		
		

		
		this.globalPort = new DefaultDockingPort();
		this.globalPort.setPreferredSize(new Dimension(100, 100));
		this.globalPort.setBorder(new LineBorder(Color.pink));
		
		
		JPanel dockableHousing = new JPanel();
		dockableHousing.setLayout(new BoxLayout(dockableHousing, BoxLayout.PAGE_AXIS));
		
		DockableTitleBar lblNewLabel = new DockableTitleBar("JTree");
		lblNewLabel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		dockableHousing.add(lblNewLabel);
		
		final JPanel housingPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		//housingPane.setBorder(null);
		housingPane.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		housingPane.setBorder(new LineBorder(Color.green));
		housingPane.add(qPane);
		dockableHousing.add(housingPane);
		Dockable newPanelDockable = DockingManager.registerDockable(dockableHousing, "JTree");
		
		newPanelDockable.getDragSources().add(lblNewLabel);
		
		DockingManager.dock(dockableHousing, (DockingPort)globalPort);
		
		qPane.setPreferredSize(new Dimension(housingPane.getWidth(), housingPane.getHeight()));
		
		this.testPanel = qPane;
		
		DockableEditorPane dep = new DockableEditorPane("Test", "test 2", globalPort, new File(Constants.changesFile));
		
		//this.testPanel2 = dep.getEditorPane();
		
		contentPane.add(globalPort, BorderLayout.CENTER);
		this.setContentPane(contentPane);
	}
	
	public JScrollPane testPanel;
	//public EditorPane testPanel2;
	
	public static void setLaF(String laf) {
		if(laf != null) {
			for(Frame frame : JFrame.getFrames()) {
				if(frame.getTitle().equals("CodeRabbit")) {
					try {
						UIManager.setLookAndFeel(laf);
						SwingUtilities.updateComponentTreeUI(frame);
						//frame.pack();
						
						Properties config = new Properties();
						config.load(new FileReader(Constants.propertiesFile));
						config.setProperty("look_and_feel", laf);
						OutputStream os = new FileOutputStream(Constants.propertiesFile);
						config.store(os, null);
						os.close();
						
						System.out.println("Setting look and feel to: " + config.getProperty("look_and_feel") + " @ " + laf);
					} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException | IOException e) {
						e.printStackTrace();// TODO Auto-generated catch block
					}
				}
			}
		}
	}

	
	public static Logger getLogger() {
		return logger;
	}
	
	public Properties getConfig() {
		return this.config;
	}
	
	public JMenuBar menuBar;
	private void createMenu() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");

		JMenuItem mntmOpen = new JMenuItem("Open File");
		mntmOpen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent ev) {
				int returnVal = fileChooser.showOpenDialog(tabbedPane.getParent());
				if(returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					EditorPane editorPane = new EditorPane(file);
					tabbedPane.addTab(file.getName(), editorPane);
					editorPane.setHasSaveLocation(true);
					editorPane.setSaved(true);
					editorPane.requestFocusInWindow();
					tabbedPane.setSelectedIndex(tabbedPane.indexOfComponent(editorPane));
					System.out.println("Opening: " + file.getAbsolutePath()); // TODO: log file
				} else {
					System.err.println("File open select cancelled."); // TODO: log file
				}
			}
		});

		JMenuItem mntmNewFile = new JMenuItem("New File");
		mntmNewFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent ev) {
				int returnVal = fileChooser.showSaveDialog(tabbedPane.getParent());

				if(returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					// tabbedPane.addTab("*"+file.getName(), new
					// EditorPane(file, tabbedPane));
					EditorPane editorPane = new EditorPane(file);
					tabbedPane.addTab("*" + file.getName(), editorPane);
					editorPane.setHasSaveLocation(true);
					editorPane.setSaved(false);
					editorPane.requestFocusInWindow();
					tabbedPane.setSelectedIndex(tabbedPane.indexOfComponent(editorPane));
					System.out.println("Opening new file: " + file.getAbsolutePath());
				} else {
					System.err.println("New file select cancelled.");
				}
			}
		});

		JMenu mnView = new JMenu("View");

		JMenu mnApplicationTheme = new JMenu("Application Theme");

		JMenu mnHighlighterTheme = new JMenu("Highlighter Theme");

		JMenuItem mntmDark = new JMenuItem("Dark");
		// mnView.add(new JMenuItem(("Dark", "/dark.xml")));

		JMenu mnSyntaxHighlighter = new JMenu("Syntax Highlighter");

		JMenuItem mntmPhp = new JMenuItem("PHP");
		mntmPhp.setSelected(true);

		JMenu mnCollaboration = new JMenu("Collaboration");

		JMenuItem mntmServer = new JMenuItem("Server");
		mntmServer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				// new Server(editorPane);
			}
		});

		JMenuItem mntmClient = new JMenuItem("Client");
		mntmClient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				// new Client("localhost");
			}
		});
		
		
		List<String> lafs = new ArrayList<String>();
		
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceAutumnLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceBusinessLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceBusinessBlackSteelLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceBusinessBlueSteelLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceCeruleanLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceChallengerDeepLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceCremeLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceCremeCoffeeLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceDustLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceDustCoffeeLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceEmeraldDuskLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceGeminiLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceGraphiteLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceMagellanLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceMarinerLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceMistAquaLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceMistSilverLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceModerateLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceNebulaLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceNebulaBrickWallLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceOfficeBlack2007LookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceOfficeBlue2007LookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceOfficeSilver2007LookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceSaharaLookAndFeel");
		lafs.add("org.pushingpixels.substance.api.skin.SubstanceTwilightLookAndFeel");
		
		Iterator<String> lafIterator = lafs.iterator();
		while (lafIterator.hasNext()) {
			final String laf = lafIterator.next();
			String lafName = laf.replace("org.pushingpixels.substance.api.skin.Substance", "").replace("LookAndFeel", "");
			JMenuItem button = new JMenuItem(lafName);
			button.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e1) {
					System.out.println("Triggered");
					setLaF(laf);
				}
			});
			mnApplicationTheme.add(button);
		}
		
		//File menu
		menuBar.add(mnFile);
		mnFile.add(mntmOpen);
		mnFile.add(mntmNewFile);
		
		
		//View menu
		menuBar.add(mnView);
		mnView.add(mnApplicationTheme);
		mnView.add(mnHighlighterTheme);
		mnHighlighterTheme.add(mntmDark);
		mnView.add(mnSyntaxHighlighter);
		mnSyntaxHighlighter.add(mntmPhp);
		
		//Collab Menu
		menuBar.add(mnCollaboration);
		mnCollaboration.add(mntmServer);
		mnCollaboration.add(mntmClient);

	}
}
