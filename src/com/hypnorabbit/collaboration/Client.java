package com.hypnorabbit.collaboration;

import java.io.*;
import java.net.*;

import com.hypnorabbit.EditorPane;

public class Client {
	
	public Socket requestSocket;
	public ObjectOutputStream out;
	public ObjectInputStream in;
	public String message;
	public EditorPane editorPane;

	public Client(String ip, EditorPane editorPane) {
		this.editorPane = editorPane;
		try {
			// 1. creating a socket to connect to the server
			requestSocket = new Socket(ip, 6464);
			System.out.println("Connected to " + ip + " in port 6464");
			// 2. get Input and Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			// 3: Communicating with the server
			do {
				try {
					message = (String) in.readObject();
					editorPane.getTextArea().append("\nServer> " + message);
					System.out.println("server>" + message);
					sendData("Hi my server");
					//message = "bye";
					//sendData(message);
				} catch (ClassNotFoundException classNot) {
					System.err.println("data received in unknown format");
				}
			} while (!message.equals("bye"));
		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			// 4: Closing connection
			try {
				in.close();
				out.close();
				requestSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	public void sendData(String data) {
		try {
			out.writeObject(data);
			out.flush();
			System.out.println("client>" + data);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}
}
