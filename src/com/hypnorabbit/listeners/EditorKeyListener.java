package com.hypnorabbit.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import javax.swing.JFileChooser;
import javax.swing.JTabbedPane;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import com.hypnorabbit.EditorPane;

public class EditorKeyListener implements KeyListener {
	
	private EditorPane editorPane;
	private RSyntaxTextArea textArea;
	private File file;
	
	public EditorKeyListener(final EditorPane editorPane, File file) {
		this.editorPane = editorPane;
		this.file = file;
		this.textArea = this.editorPane.getTextArea();
	}
	
	@Override
	public void keyPressed(KeyEvent ev) {
		if(ev.getKeyCode() != KeyEvent.VK_END && ev.getKeyCode() != KeyEvent.VK_INSERT && ev.getKeyCode() != KeyEvent.VK_NUM_LOCK && ev.getKeyCode() != KeyEvent.VK_UP && ev.getKeyCode() != KeyEvent.VK_DOWN && ev.getKeyCode() != KeyEvent.VK_RIGHT && ev.getKeyCode() != KeyEvent.VK_LEFT && ev.getKeyCode() != KeyEvent.VK_CONTROL && ev.getKeyCode() != KeyEvent.VK_ALT && ev.getKeyCode() != KeyEvent.VK_SHIFT && ev.getKeyCode() != KeyEvent.VK_CAPS_LOCK) {
			if(this.editorPane.isSaved()) {
				this.editorPane.setSaved(false);
				//this.tabbedPane.setTitleAt(this.tabbedPane.indexOfTab(this.file.getName()), "*"+this.file.getName());
			}
		}

	}
	
	@Override
	public void keyReleased(KeyEvent ev) {
		//System.out.println(textArea.getText(textArea.getLineStartOffsetOfCurrentLine(), textArea.getLineEndOffsetOfCurrentLine()-textArea.getLineStartOffsetOfCurrentLine()));
		if(ev.isControlDown()) {
			switch(ev.getKeyCode()) {
			case KeyEvent.VK_S:
				System.out.println("KeyCombo CTRL+S Pressed");
				if(!this.editorPane.isSaved() && this.editorPane.hasSaveLocation()) {
					try {
						PrintStream ps = new PrintStream(this.file.getAbsoluteFile());
				        ps.print(this.textArea.getText());
				        ps.close();
				        //this.tabbedPane.setTitleAt(this.tabbedPane.indexOfTab("*"+this.file.getName()), this.file.getName());
				        this.editorPane.setSaved(true);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				} else {
					try {
						JFileChooser fr = new JFileChooser();
					    int fileChosen = fr.showSaveDialog(this.editorPane);

					    if(fileChosen == JFileChooser.APPROVE_OPTION) {
							PrintStream ps = new PrintStream(fr.getSelectedFile());
					        ps.print(this.textArea.getText());
					        ps.close();
					        
					        //this.tabbedPane.setTitleAt(this.tabbedPane.indexOfTab("*"+this.file.getName()), this.file.getName());
					        this.editorPane.setSaved(true);
					        this.editorPane.setHasSaveLocation(true);
					        System.out.println("File saved: " + file.getName() + "@" + this.file.getAbsolutePath().replace(this.file.getName(), ""));
					    } else {
					    	System.err.println("File save cancelled by user.");
					    }
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
				break;
			case KeyEvent.VK_O:
				System.out.println("KeyCombo CTRL+O Pressed");
				
				break;
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {}

}
