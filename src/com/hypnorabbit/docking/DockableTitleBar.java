package com.hypnorabbit.docking;

import javax.swing.JLabel;

import org.pushingpixels.substance.api.DecorationAreaType;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;

public class DockableTitleBar extends JLabel {
	
	private static final long serialVersionUID = 1L;
	
	public DockableTitleBar(String text) {
		super(text);
		SubstanceLookAndFeel.setDecorationType(this, DecorationAreaType.HEADER);
        this.setOpaque(true);
	}
	
    public void doLayout() {
    	//this.setSize(new Dimension(this.getParent().getWidth(), 25));
        this.setBounds(this.getParent().getX(), this.getParent().getY(), this.getParent().getWidth(), 25);//his.setBounds(ownerInsets.left, ownerInsets.top, ownerWidth-ownerInsets.left-ownerInsets.right, 25);
        this.setAlignmentX(this.getParent().getAlignmentX());
        this.setAlignmentY(this.getParent().getAlignmentY());
    }
}
