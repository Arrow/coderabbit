package com.hypnorabbit;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.flexdock.docking.Dockable;
import org.flexdock.docking.DockingConstants;
import org.flexdock.docking.DockingManager;
import org.flexdock.docking.defaults.DefaultDockingPort;

public class MovablePanel extends JPanel implements DockingConstants {

    private JLabel titlebar;
    private Dockable dockableImpl;
    
    
    public static EditorPane createContentPane() {
        EditorPane p = new EditorPane(new File("C:\\Users\\Andrew\\Documents\\codeRabbit\\changes.txt"));

        p.add(buildDockingPort("North"), BorderLayout.NORTH);
        //p.add(buildDockingPort("South"), BorderLayout.SOUTH);
        //p.add(buildDockingPort("East"), BorderLayout.EAST);
        //p.add(buildDockingPort("West"), BorderLayout.WEST);
        //p.add(createDockingPort("Center"), BorderLayout.CENTER);
        return p;
    }
    
    
    private static DefaultDockingPort buildDockingPort(String desc) {
        // create the DockingPort
        DefaultDockingPort port = createDockingPort(desc);

        // create the Dockable panel
        MovablePanel cd = new MovablePanel();
        DockingManager.registerDockable(cd.getDockable());

        // dock the panel and return the DockingPort
        port.dock(cd.getDockable(), CENTER_REGION);
        return port;
    }

    private static int getTabPosition(String desc) {
        if ("North".equals(desc))
            return JTabbedPane.TOP;
        if ("South".equals(desc))
            return JTabbedPane.BOTTOM;
        if ("East".equals(desc))
            return JTabbedPane.RIGHT;
        if ("West".equals(desc))
            return JTabbedPane.LEFT;
        return JTabbedPane.TOP;
    }

    private static DefaultDockingPort createDockingPort(String desc) {
        DefaultDockingPort port = new DefaultDockingPort();
        port.setBackground(Color.gray);
        port.setPreferredSize(new Dimension(500, 500));
        port.getDockingProperties().setTabPlacement(getTabPosition(desc));
        return port;
    }
    
    private Dockable getDockable() {
        return dockableImpl;
    }
}
